#include <stdio.h>
#include <stdlib.h>

/* ========== GLOBAL VARIABLES */
char string[11][64][64];
long number[11][64];

int ctr, ctr2, ctr3, entrylimit, problem;

int container = 0;
int pointer = 0;

char tempstr[64];
int temp = 0, templen;

/* ========== COMMANDS */
void sortstring();
void askstring();
void showstring();
void askinteger();
int checkodd(int);
void title();

/* ========== PROBLEM 10 */
void main()
{
	problem = 10; entrylimit = 26; title(); entrylimit--;

	askstring();

	sortstring();

	showstring();

	getch();
}

/* ==================== COMMANDS ==================== */
void sortstring()
{
	for (ctr2 = 0; ctr2 <= entrylimit; ctr2++)
	{
		for (ctr = ctr2+1; ctr <= entrylimit; ctr++)
		{
			if (string[problem][ctr2][0] > string[problem][ctr][0])
			{
				for (ctr3 = 0; ctr3 <= 64-1; ctr3++)
				{
					tempstr[ctr3] = string[problem][ctr2][ctr3];
				}

				for (ctr3 = 0; ctr3 <= 64-1; ctr3++)
				{
					string[problem][ctr2][ctr3] = string[problem][ctr][ctr3];
				}

				for (ctr3 = 0; ctr3 <= 64-1; ctr3++)
				{
					string[problem][ctr][ctr3] = tempstr[ctr3];
				}
			}
		}
	}
}

void askstring()
{
	printf("\n");
	printf("ENTER ENTRY:\n");
	printf("\n");

	for (ctr = 0; ctr <= entrylimit; ctr++)
	{
		printf("%d ENTRY: ",ctr+1); gets(string[problem][ctr]);
	}
}

void showstring()
{
	printf("\n");
	printf("SHOWING ENTRY:\n");
	printf("\n");

	for (ctr = 0; ctr <= entrylimit; ctr++)
	{
		printf("%d ENTRY: ",ctr+1);
		printf("%s\n",string[problem][ctr]);
	}

}

void askinteger(int method)
{
	int ctr;

	printf("\n");
	printf("ENTER ENTRY: ");
	printf("\n");

	for (ctr = 0; ctr <= entrylimit; ctr++)
	{
		printf("\n %d ENTRY: ", ctr+1);
		if (method == 0) string[problem][ctr][0] = getche();
		else if (method == 1) gets(string[problem][ctr]);
		else if (method == 2) scanf("%d",&number[problem][ctr]);
	}

	if (method != 2)
	{
	for (ctr = 0; ctr <= entrylimit; ctr++)
	{
		if (string[problem][ctr][0] != '*') number[problem][ctr] = atoi(string[problem][ctr]);
		else number[problem][ctr] = 99;
	}
	}

}

int checkodd(int number)
{
	if (number == 99) return number;
	else return (number % 2);
}

void title()
{
	clrscr();
	printf("\n");
	printf("PROBLEM %d \n",problem);
	printf("\n");
}